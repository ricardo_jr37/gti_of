class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
	enum kind: [:member, :manager]
  	enum status: [:active, :inactive]
  	has_many :gamifications
  	has_many :member_allocations
  	has_many :member_allocations
  	has_many :projects, through: :member_allocations

end
