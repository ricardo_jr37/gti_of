class Project < ApplicationRecord
	enum status: [:active, :inactive]
  	
  	has_many :member_allocations
  	has_many :users, through: :member_allocations
end
