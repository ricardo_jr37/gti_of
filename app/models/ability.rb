# app/models/ability.rb
class Ability
  include CanCan::Ability
  def initialize(user)
    if user
      if user.kind == 'member'
        can :access, :rails_admin
        can :dashboard
        can :read, Gamification, user_id: user.id
        can :read, Project, user_id: user.id
        #can :manage, User, user_id: user.id
        can :manage, User, id: user.id
        cannot :index, User
      elsif user.kind == 'manager'
        can :manage, :all
      end
    end
  end
end