class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name
      t.date :project_start
      t.date :project_end
      t.references :user, foreign_key: true
      t.text :description
      t.integer :status

      t.timestamps
    end
  end
end
