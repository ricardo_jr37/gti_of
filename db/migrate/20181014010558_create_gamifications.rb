class CreateGamifications < ActiveRecord::Migration[5.2]
  def change
    create_table :gamifications do |t|
      t.integer :point
      t.text :note
      t.references :user, foreign_key: true
      t.integer :user_id

      t.timestamps
    end
  end
end
