class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :sobrenome
      t.string :cargo
      t.string :number
      t.integer :kind
      t.timestamps
    end
  end
end
